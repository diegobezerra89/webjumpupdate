# Documentação Básica
Realização do teste Webjump Front-end

##Update
Este se trata de um update, pois recebi a oportunidade de realizar os ajustes faltantes do primeiro submit do teste.

## O projeto
Utilizei HTML, CSS e JS.
O Sass foi utilizado para dar modularização e facilitar o desenvolvimento do CSS.
Utilizei o bundler Webpack, decidi por utilizá-lo pois em aplicações reais
há sempre a possibilidade de crescimento e implementação de outras tecnologias 
que necessitam dessa ferramenta, tais como React, VueJs e etc.

Foi a minha primeira experiência com uma simulação de projeto real,
me senti bastante desafiado e ao meu ver consegui um resultado razoável.

### Metodologia utilizada
- Desktop First
- Commit em inglês para facilitar a leitura e possível equipe com estrangeiros
- BEM - Block Element Modifier (CSS)
- Modularização de código CSS e JS (Sass e Webpack)

### Desenvolvimento
>>(Comentários do primeiro envio)
Ao me deparar com o desafio percebi que as minhas habilidades de Javascript seriam testadas,
então, como tenho uma maior intimidade com o front, decidi focar inicialmente no layout,
porém, adotei a tática da metodologia errada, deveria ter utilizado "mobile first", pois desenvolvi toda a interface
de desktop e me deparei com o código quebrando ao tentar realizar as media queries. Com certeza foi uma escolha equivocada,
fui me complicando cada vez mais, e por fim decidi manter como um quesito onde eu falhei, deveria ter administrado melhor
o meu tempo.

Encontrei dificuldades em configurar o webpack-dev-server, pois o App.js estava utilizando a porta 8888 e não consegui encontrar 
uma solução para implementar o dev-server, posso estar enganado aliás. Este erro me custou ter que compilar o bundle toda vez que fazia
modificações no index.html, pois estava utilizando o plugin HtmlWebpackPlugin para gerar dois arquivos, um de dev e um de produção.

Utilizei o Github para realizar o desafio pois havia no meu ambiente de desenvolvimento o bloqueio ao bitbucket. Realizei a importação
do repositótio do Github para o Bitbucket.

>>(comentários do segundo envio)
Ao perceber que falhei na execução das Media Queries, foquei em obter o conhecimento das melhores técnicas para aplicação destas.
Realizei um método de fazer um mixin com vários ifs, onde cada if é a representação de uma medida das media queries, e incluía este mixin onde eu via a necessidade de formatação.
Consegui formatar as media Queries de maneira bastante fácil com este método.

Outro fator faltante no primeiro envio foi o filtro de cores, realizei o desenvolvimento dessa função com sucesso. (Curti demais quando consegui! Rs)



### Instruções
- Instale as dependências
```
npm install
```
- Inicie a aplicação
```
npm start
```
- Para compilar o Sass
```
npm run compile:sass
```
- Para Gerar o bundle Webpack (Sempre que modificar os Js ou Html)
```
npm run dev
```

### Agradecimentos
Agradeço à equipe da Webjump por me dar a oportunidade de mostrar meu potencial com uma segunda oportunidade de realizar o teste, creio que consegui realizar o teste dentro das minhas capacidades.
Adorei fazer parte deste processo, e com certeza somou muito à minha caminhada em busca do desenvolvimento profissional.